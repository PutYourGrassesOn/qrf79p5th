# Horsen

This is the final project from this [tutorial](https://www.youtube.com/watch?v=7T_xVBwFdJA) with some functionalities added/changed.

## Environment

This project was developed using Python 3.10.1.
Besides the built-in Python 3 modules, the following modules were used.

1) [pyinput](https://pynput.readthedocs.io/en/latest/index.html) - `pip install pyinput`
2) [pyautogui](https://pyautogui.readthedocs.io/en/latest/) - `pip install pyautogui`
3) [pyinstaller](https://pyinstaller.readthedocs.io/en/stable/index.html) - `pip install pyinstaller`

pyautogui requires the installation of Pillow to take screenshots - `pip install Pillow`.

## Getting started

Adjust the IP address in the files.
Convert the backdoor.py and the keylogger.py to an executable using the pyinstaller, for example.

`pyinstaller backdoor.py --onefile --noconsole`

Start the server and run the executable inside the client machine.

## Documentation

This file contains a short description of the reverse shell built-in functions. 

### Backdoor-Server Behaviour

Run the server.py file.
1) Creates a socket object ('server') that will listen for connections

Run the backdoor.py file.
1) Creates a socket object ('connection') that will connect to the server
2) Once a connection is established, it tries to create the folder ('keylogger') inside the AppData and send the result of the operation to the server - this happens before the 'while True' loop in both ends.
3) It starts the reverse shell

### Reverse Shell Commands

The reverse shell contains multiple options/commands built-in.

#### help
Prints the help menu which contains all commands built into the reverse shell.
It only affects the server-side.

#### clear
Clears the terminal screen. It only affects the server-side and is only useful inside a terminal.

#### verbose
Inverts the value stored inside the `verbose_value` variable.
In the client-side the backdoor will not have a console so, the outputs are only useful for testing/debugging.

#### cd *DirectoryName*
Changes the directory in the client-side.

It returns nothing if the process was successfully completed, otherwise, it returns the exception found.
The result of this operation should be seen in the prompt for a new command.

#### quit
Closes the program in both sides.

It has the same behaviour as *sleep* in the server-side - closes the program.
In the client-side, it ends the `main` function and returns 0 which ends the `while True` loop and also closes the program.

#### sleep
Closes the program in the server-side only.

It has the same behaviour as *quit* in the server-side - closes the program.
In the client-side, it ends the `main` function returning 1 which will keep the `while True` loop running - the program will keep attempting to connect with the server after a random period of time.

#### upload *FileName*
Tries to upload the specified file into the client machine.

First it verifies if the file exist inside the server files, if it does not, it will print an error message.
If the file exists, the command is sent to the client and the operation starts:
1) It sends the file name and waits for the client confirmation (`RECEIVED`)
2) It sends the file content and waits for the client confirmation (`RECEIVED`)
3) It sends the file sha256 hash and waits for the client confirmation (`RECEIVED`)

If any of these steps fails, both ends restart.

#### download *FileName*
Tries to download a file from the client machine.

Unlike in the *upload*, here the request/command is sent immediately to the client which will verify if the file exists.
If the file does not exist, the operation ends printing the exception else, it will try to send the file name, content and sha256 hash from the client to the server.
It follows the same data-response architecture explained in the *upload* command.

Both ends restart if an error occurs during this last stage.

#### screenshot
It takes a screenshot in the client machine and downloads it.

By default, it takes the screenshot and temporarily stores it inside the 'AppData\Roaming\keylogger' folder.
Then it downloads the image from the client to the server using the methods from the *download* command.

#### keylog_start
Starts the keylogger.

By default, the keystrokes are stored inside the AppData\Roaming\keylogger\keylogger.txt file.

#### keylog_dump
Downloads the file containing the keystrokes from the client to the server.

First is checks if the AppData\Roaming\keylogger\keylogger.txt file exists.
If it does not, the keylogger was not started yet and this file can not be downloaded.
If the file exists, it uses the methods from the 'download' command to download the file.

#### keylog_stop
Stops the keylogger and deletes the file where the keystrokes are stored from the client machine.

First is checks if the AppData\Roaming\keylogger\keylogger.txt file exists.
If it does not, the keylogger was not started yet.
If the file exists, the `pynput.keyboard.Listener.stop` is called to stop the `Listener` object - read pyinput documentation for more information about this.
Finally, using `os.remove()` the file is removed from the system.

#### persistence *RegistryName* *FileName*
If the client is running a Windows OS, it adds a new registry value inside the 'HKCU\Software\Microsoft\Windows\CurrentVersion\Run' which makes the backdoor persistent - it automatically runs when the client is restarted.

By default, it copies the executable of the backdoor to the AppData\Roaming\keylogger\ and uses this executable in the registry - if the user deletes the original file, the backdoor will still be active.
It prints the command executed or the error encountered.

## Future Work

Manage multiple clients with the same server.
Encrypt the communication to prevent discovering it with Wireshark.